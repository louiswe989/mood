export interface Mood {
    id?: string;
    mood: string;
    created_on: string;
    description: string;
}
