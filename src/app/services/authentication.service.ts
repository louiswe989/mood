import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {
  url = 'http://127.0.0.1:5000';

  postOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Access-Control-Allow-Origin': this.url,
    })
  };

  constructor(private http: HttpClient) { }

  setJwtToken(authResult) {
    localStorage.setItem('access_token', authResult.access_token);
    localStorage.setItem('refresh_token', authResult.refresh_token);
  }

  getJwtToken() {
    const jwtToken = {
      accessToken: localStorage.getItem('access_token'),
      refreshToken: localStorage.getItem('refresh_token')
    };

    return jwtToken;
  }

  clearJwtToken() {
    localStorage.removeItem('access_token');
    localStorage.removeItem('refresh_token');
  }

  refreshAccessToken() {
    return this.http.post<any>(this.url + '/refresh', {}, {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Access-Control-Allow-Origin': this.url,
        Authorization: `Bearer ${localStorage.getItem('refresh_token')}`
      })
    });
  }
}
