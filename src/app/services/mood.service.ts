import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Mood } from '../interfaces/mood';

@Injectable({
  providedIn: 'root'
})
export class MoodService {
  url = 'http://127.0.0.1:5000';

  postOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Access-Control-Allow-Origin': this.url,
    })
  };

  constructor(private http: HttpClient) { }

  addMood(mood: Mood): Observable<Mood> {
    return this.http.post<Mood>(this.url + '/mood', mood, this.postOptions);
  }

  getMoods(): Observable<Mood[]> {
    return this.http.get<Mood[]>(this.url + '/moods');
  }

  getMood(moodId: string): Observable<Mood> {
    return this.http.get<Mood>(this.url + '/mood/' + moodId);
  }
}
