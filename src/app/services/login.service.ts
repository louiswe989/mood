import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { tap, shareReplay } from 'rxjs/operators';
import { AuthenticationService } from './authentication.service';

@Injectable({
  providedIn: 'root'
})
export class LoginService {
  url = 'http://127.0.0.1:5000';

  postOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Access-Control-Allow-Origin': this.url,
      'X-NEED-JWT': ''
    })
  };

  jwtOptions = {
    headers: new HttpHeaders({
      'X-NEED-JWT': ''
    })
  };

  constructor(private http: HttpClient, private authenticationService: AuthenticationService) {}

  login(userLoginDetails: any): Observable<any> {
    return this.http.post(this.url + '/login', userLoginDetails, this.postOptions).pipe(
      tap(response => this.authenticationService.setJwtToken(response)),
      shareReplay()
    );
  }

  logout() {
    this.authenticationService.clearJwtToken();
  }

  testJwt(): Observable<any> {
    return this.http.get(this.url + '/protected', this.jwtOptions);
  }

  testRefreshJwt(): Observable<any> {
    const request = this.http.post(this.url + '/refresh', {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Access-Control-Allow-Origin': this.url,
        'X-Need-Refresh-Token': ''
      })
    }).pipe(
      tap(console.log)
    );

    return request;
  }
}
