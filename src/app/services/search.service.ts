import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SearchService {
  url = 'http://127.0.0.1:5000';

  postOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Access-Control-Allow-Origin': this.url,
    })
  };

  constructor(private http: HttpClient) { }

  searchMoods(keyword: any): Observable<any> {
    return this.http.post<any>(this.url + '/mood/search', keyword, this.postOptions);
  }
}
