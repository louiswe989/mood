import { Component, ViewChild, ElementRef, AfterViewInit, OnInit, OnDestroy } from '@angular/core';
import { ApexAxisChartSeries, ApexChart, ApexResponsive, ApexNonAxisChartSeries, ChartComponent } from 'ng-apexcharts';
import { MoodService } from '../services/mood.service';
import { map, tap } from 'rxjs/operators';
import { DatePipe } from '@angular/common';
import { Subscription } from 'rxjs';
import { NotifierService } from '../services/notifier.service';
import { Label, SingleDataSet, MultiDataSet } from 'ng2-charts';
import { ChartType } from 'chart.js';

export type ChartOptions = {
  series: ApexNonAxisChartSeries;
  chart: ApexChart;
  responsive: ApexResponsive[];
  labels: any;
};
@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss'],
  providers: [DatePipe]
})
export class Tab2Page implements OnInit, OnDestroy{
  statisticData = [];
  statisticLabels = [
    'Exciting',
    'Suprise',
    'Happy',
    'Confused',
    'Angry',
    'Sad',
    'Crying'
  ];

  // for apexcharts
  @ViewChild('chart') chart: ChartComponent;
  public chartOptions: Partial<ChartOptions>;

  //for chart.js
  // public chartLabels: Label[] = this.statisticLabels;
  // public chartData: SingleDataSet = [];
  // public chartType: ChartType = 'doughnut';
  public doughnutChartLabels: Label[] = this.statisticLabels;
  public doughnutChartData: SingleDataSet = [];
  public doughnutChartType: ChartType = 'doughnut';

  moods = [];
  highCountMood;
  date = new Date();

  notification: Subscription;

  constructor(
    private datePipe: DatePipe,
    private moodService: MoodService,
    private notifierService: NotifierService
  ) {
    this.chartOptions = {
      series: [],
      chart: {
        type: 'donut'
      },
      labels: this.statisticLabels,
      responsive: [
        {
          breakpoint: 480,
          options: {
            chart: {
              width: '100%'
            },
            legend: {
              position: 'bottom'
            }
          }
        }
      ]
    };
  }

  ngOnInit() {
    this.doRefresh();

    this.notification = this.notifierService.moodsUpdated.subscribe(data => {
      if (data && data === 'SUCCESS') {
        this.doRefresh();
      }
    });
  }

  ngOnDestroy() {
    this.notification.unsubscribe();
  }

  doRefresh() {
    this.moodService.getMoods().pipe(
      tap(moods => this.moods = moods)
    ).subscribe(() => this.getStatistic(this.date));
  }

  getStatistic(date) {
    this.statisticData = [];

    const filteredMoods = this.moods.filter(mood =>
      this.datePipe.transform(mood.created_on, 'yyyy-MM', '+0000')
      === this.datePipe.transform(date, 'yyyy-MM', '+0800'));

    this.statisticLabels.forEach(label => {
      const count = filteredMoods.filter(mood => mood.mood === label).length;
      this.statisticData.push(count);
    });

    this.highCountMood = this.statisticLabels[this.statisticData.indexOf(Math.max(...this.statisticData))];

    this.chartOptions.series = this.statisticData;
    this.doughnutChartData = this.statisticData;
  }
}
