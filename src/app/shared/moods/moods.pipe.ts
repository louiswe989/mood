import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'moods'
})
export class MoodsPipe implements PipeTransform {

  transform(value: string): string {
    const moods = [
      {
        mood: 'Exciting',
        img: 'assets/icon/exciting.png'
      },
      {
        mood: 'Suprise',
        img: 'assets/icon/suprise.png'
      },
      {
        mood: 'Happy',
        img: 'assets/icon/happy.png'
      },
      {
        mood: 'Confused',
        img: 'assets/icon/confused.png'
      },
      {
        mood: 'Angry',
        img: 'assets/icon/angry.png'
      },
      {
        mood: 'Sad',
        img: 'assets/icon/sad.png'
      },
      {
        mood: 'Crying',
        img: 'assets/icon/crying.png'
      },
    ];

    const foundMood = moods.find(mood => mood.mood === value);

    return foundMood.img;
  }

}
