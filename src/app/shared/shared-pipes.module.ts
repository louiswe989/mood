import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MoodsPipe } from './moods/moods.pipe';



@NgModule({
  declarations: [MoodsPipe],
  imports: [
    CommonModule
  ],
  exports: [MoodsPipe]
})
export class SharedPipesModule { }
