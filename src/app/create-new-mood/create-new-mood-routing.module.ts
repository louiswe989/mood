import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CreateNewMoodPage } from './create-new-mood.page';

const routes: Routes = [
  {
    path: '',
    component: CreateNewMoodPage
  },
  {
    path: 'mood-description',
    loadChildren: () => import('./mood-description/mood-description.module').then( m => m.MoodDescriptionPageModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CreateNewMoodPageRoutingModule {}
