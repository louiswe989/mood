import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AlertController, NavController, LoadingController, ToastController } from '@ionic/angular';
import { Mood } from 'src/app/interfaces/mood';
import { MoodService } from 'src/app/services/mood.service';
import { NotifierService } from 'src/app/services/notifier.service';

@Component({
  selector: 'app-mood-description',
  templateUrl: './mood-description.page.html',
  styleUrls: ['./mood-description.page.scss'],
})
export class MoodDescriptionPage implements OnInit {
  mood: string;
  description: string;

  loading: HTMLIonLoadingElement;

  constructor(
    public alertCtrl: AlertController,
    public loadingCtrl: LoadingController,
    public toastCtrl: ToastController,
    private route: ActivatedRoute,
    private navCtrl: NavController,
    private moodService: MoodService,
    private notifierService: NotifierService
  ) { }

  ngOnInit() {
    this.mood = this.route.snapshot.paramMap.get('mood');
  }

  addNewMood() {
    const mood: Mood = {
      mood: this.mood,
      created_on: new Date().toLocaleString(),
      description: this.description
    };

    this.presentLoading();
    this.moodService.addMood(mood).subscribe({
      next: () => {
        this.notifierService.moodsUpdated.next('SUCCESS');
        this.presentAlert('New Mood Has Been Added!', 'success-alert');
      },
      error: (err) => {
        this.dismissLoading();
        this.presentToast(
          err.status + ': ' + err.error.error,
          'danger'
        );
      },
      complete: () => {
        this.dismissLoading();
        setTimeout(() => {
          this.alertCtrl.dismiss();
        }, 2000);
      }
    });
  }

  async presentLoading() {
    this.loading = await this.loadingCtrl.create({
      spinner: 'dots',
      duration: 4000,
      message: 'Please wait...',
      translucent: true
    });
    return await this.loading.present();
  }

  async dismissLoading() {
    return await this.loading.dismiss();
  }

  presentToast(message: string, color: 'danger' | 'success') {
    this.toastCtrl
      .create({
        message,
        duration: 5000,
        position: 'top',
        color,
        animated: true,
        buttons: [
          {
            text: 'Close',
            role: 'cancel'
          }
        ]
      })
      .then(toast => toast.present());
  }

  async presentAlert(header: string, cssClass: string) {
    const alert = await this.alertCtrl.create({
      cssClass,
      header,
      backdropDismiss: false
    });

    alert.onDidDismiss().then(() => {
      this.navCtrl.navigateBack('/');
    });

    await alert.present();
  }

}
