import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { MoodDescriptionPageRoutingModule } from './mood-description-routing.module';

import { MoodDescriptionPage } from './mood-description.page';
import { SharedPipesModule } from 'src/app/shared/shared-pipes.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MoodDescriptionPageRoutingModule,
    SharedPipesModule
  ],
  declarations: [MoodDescriptionPage]
})
export class MoodDescriptionPageModule {}
