import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MoodDescriptionPage } from './mood-description.page';

const routes: Routes = [
  {
    path: '',
    component: MoodDescriptionPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MoodDescriptionPageRoutingModule {}
