import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CreateNewMoodPageRoutingModule } from './create-new-mood-routing.module';

import { CreateNewMoodPage } from './create-new-mood.page';
import { SharedPipesModule } from '../shared/shared-pipes.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CreateNewMoodPageRoutingModule,
    SharedPipesModule
  ],
  declarations: [CreateNewMoodPage]
})
export class CreateNewMoodPageModule {}
