import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-create-new-mood',
  templateUrl: './create-new-mood.page.html',
  styleUrls: ['./create-new-mood.page.scss'],
})
export class CreateNewMoodPage implements OnInit {
  moods = [
    'Exciting',
    'Suprise',
    'Happy',
    'Confused',
    'Angry',
    'Sad',
    'Crying'
  ];

  // {
  //   mood: 'Exciting',
  //   img: 'assets/icon/exciting.png'
  // },
  // {
  //   mood: 'Suprise',
  //   img: 'assets/icon/suprise.png'
  // },
  // {
  //   mood: 'Happy',
  //   img: 'assets/icon/happy.png'
  // },
  // {
  //   mood: 'Confused',
  //   img: 'assets/icon/confused.png'
  // },
  // {
  //   mood: 'Angry',
  //   img: 'assets/icon/angry.png'
  // },
  // {
  //   mood: 'Sad',
  //   img: 'assets/icon/sad.png'
  // },
  // {
  //   mood: 'Crying',
  //   img: 'assets/icon/crying.png'
  // },

  slideOpts = {
    initialSlide: 0,
    slidesPerView: 3,
    speed: 400
  };

  constructor(
    private router: Router
  ) { }

  ngOnInit() {
  }

  addMoodDescription(mood: string) {
    this.router.navigate(['/create-new-mood/mood-description', {mood}]);
  }

}
