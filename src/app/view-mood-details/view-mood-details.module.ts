import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ViewMoodDetailsPageRoutingModule } from './view-mood-details-routing.module';

import { ViewMoodDetailsPage } from './view-mood-details.page';
import { SharedPipesModule } from '../shared/shared-pipes.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ViewMoodDetailsPageRoutingModule,
    SharedPipesModule
  ],
  declarations: [ViewMoodDetailsPage]
})
export class ViewMoodDetailsPageModule {}
