import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ViewMoodDetailsPage } from './view-mood-details.page';

const routes: Routes = [
  {
    path: '',
    component: ViewMoodDetailsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ViewMoodDetailsPageRoutingModule {}
