import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { Mood } from '../interfaces/mood';
import { MoodService } from '../services/mood.service';

@Component({
  selector: 'app-view-mood-details',
  templateUrl: './view-mood-details.page.html',
  styleUrls: ['./view-mood-details.page.scss'],
})
export class ViewMoodDetailsPage implements OnInit {
  date = new Date();
  moodId = this.route.snapshot.paramMap.get('moodId');
  mood$: Observable<Mood>;

  constructor(
    private route: ActivatedRoute,
    private moodService: MoodService
  ) { }

  ngOnInit() {
    this.mood$ = this.moodService.getMood(this.moodId).pipe(tap(console.log));
  }

}
