import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    loadChildren: () => import('./tabs/tabs.module').then(m => m.TabsPageModule)
  },
  {
    path: 'view-mood-details',
    loadChildren: () => import('./view-mood-details/view-mood-details.module').then( m => m.ViewMoodDetailsPageModule)
  },
  {
    path: 'create-new-mood',
    loadChildren: () => import('./create-new-mood/create-new-mood.module').then( m => m.CreateNewMoodPageModule)
  }
];
@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule],
})
export class AppRoutingModule {}
