import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent } from '@angular/common/http';
import { AuthenticationService } from '../services/authentication.service';
import { Observable, throwError } from 'rxjs';
import { switchMap, catchError } from 'rxjs/operators';

@Injectable()
export class TokenInterceptor implements HttpInterceptor {
    constructor(private authenticationService: AuthenticationService) {}

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        if (request.headers.has('X-NEED-JWT')) {
            const jwtTokens = this.authenticationService.getJwtToken();

            return next.handle(
                request.clone({
                    headers: request.headers.delete('X-NEED-JWT').append('Authorization', `Bearer ${jwtTokens.accessToken}`)
                })
            ).pipe(
                catchError(error => {
                    if ([401, 403, 422].includes(error.status)) {
                        console.log('[JWT] Access token expired, refreshing access token');

                        return this.authenticationService.refreshAccessToken().pipe(
                            switchMap(jwtToken => {
                                this.authenticationService.setJwtToken({
                                    access_token: jwtToken.access_token,
                                    refresh_token: jwtTokens.refreshToken
                                });

                                return next.handle(request.clone({
                                    headers: request.headers.delete('X-NEED-JWT').append('Authorization', `Bearer ${jwtToken.access_token}`)
                                }));
                            }),
                            catchError(error => {
                                if ([401, 403, 422].includes(error.status)) {
                                    console.log('[JWT] Unable to refresh access token, logging out...');

                                    this.authenticationService.clearJwtToken();
                                }

                                return throwError(error);
                            })
                        );
                    }

                    return throwError(error);
                })
            );
        } else {
            return next.handle(request);
        }
    }
}
