import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { Observable, Subscription } from 'rxjs';
import { Mood } from '../interfaces/mood';
import { MoodService } from '../services/mood.service';
import { NotifierService } from '../services/notifier.service';
import { SearchService } from '../services/search.service';

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page implements OnInit, OnDestroy{
  date = new Date();
  mood$: Observable<Mood[]>;

  notification: Subscription;

  keyword: string;

  moodRecord = [
    'Happy',
    'Exciting',
    'Angry'
  ];

  constructor(
    private router: Router,
    private moodService: MoodService,
    private searchService: SearchService,
    private notifierService: NotifierService
  ) {}

  ngOnInit() {
    this.doRefresh();

    this.notification = this.notifierService.moodsUpdated.subscribe(data => {
      if (data && data === 'SUCCESS') {
        this.doRefresh();
      }
    });
  }

  ngOnDestroy() {
    this.notification.unsubscribe();
  }

  doRefresh() {
    this.mood$ = this.moodService.getMoods();
  }

  createNewMood() {
    this.router.navigate(['/create-new-mood']);
  }

  viewMoodDetails(mood: Mood) {
    const moodId: string = mood.id;
    this.router.navigate(['/view-mood-details', {moodId}]);
  }

  setFilteredMoods() {
    const keyword = {keyword: this.keyword};
    this.mood$ = this.searchService.searchMoods(keyword);
  }
}
