import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { LoginService } from '../services/login.service';

@Component({
  selector: 'app-tab3',
  templateUrl: 'tab3.page.html',
  styleUrls: ['tab3.page.scss']
})
export class Tab3Page implements OnInit{
  loginForm: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    private loginService: LoginService
  ) {
    this.initializeForm();
  }

  ngOnInit() {
  }

  initializeForm() {
    this.loginForm = this.formBuilder.group({
      username: '',
      password: ''
    });
  }

  login() {
    const userDetails: any = this.loginForm.value;

    this.loginService.login(userDetails).subscribe();
  }

  testJwt() {
    this.loginService.testJwt().subscribe();
  }

  testRefreshJwt() {
    this.loginService.testRefreshJwt().subscribe();
  }

  logout() {
    this.loginService.logout();
  }

}
